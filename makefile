cc=gcc
OPT= -Wall -pedantic -std=gnu99
CU= -lncurses
all: fonctions
	$(CC) $(OPT) fonctions.o main.c $(CU)
fonctions:
	$(CC) $(OPT) -c fonctions.c $(CU)
clean: 
	rm -f *.o
