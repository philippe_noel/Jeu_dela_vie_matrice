Impl�mentation du jeu de la vie en langage C sous la forme d'une
matrice avec la biblioth�que lncurses.

**Installation**

installer la biblioth�que lncurses (sudo apt-get install
libncurses5-dev)

Se place dans le dossier et compiler avec make.

**Utilisation**

Lancer le fichier ./a.out dans un shell bash.

**Fonctionnement**

L'automate cellulaire est bas� sur le jeu de la vie de Conway cr�e
en 1970. Chaque cellule est caract�ris� par deux �tats, "@" pour une
cellule vivante et " " pour une cellule morte. A chaque �tape de
l'�volution, l'algorithme calcule les voisins de chaque cellules. Si
une cellule poss�de 0 ou 1 voisins, alors la cellule meurt. La cellule
meurt �galement si elle poss�de plus de 4 voisins. Dans le cas o� la
cellule a exactement 2 voisins, alors elle reste au m�me �tat. Si la
cellule poss�de 3 voisins vivantes, alors elle est vivante � la
prochaine �tape.
