#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h>
#include "fonctions.h"

int main() {

  initscr();
  int nline = 50;
  int nrow = 50;

  int **nouveau_tableau = initialiserTableau(nline, nrow);
  int **tableau_temp = initialiserTableau(nline, nrow);

  randomTableau(nouveau_tableau, nline, nrow);
  afficherTableau(nouveau_tableau, nline, nrow);
  
  int nb_tour = 10000;
  
  for (int ii = 0; ii < nb_tour; ii++) {
    tableau_temp = nouveau_tableau;
    afficherTableau(nouveau_tableau, nline, nrow);
    //xprintf("la tout va bien\n");
    modifierMatrice(nouveau_tableau, tableau_temp, nline, nrow);
    sleep(0.1);
  }
  endwin();
  printf("fin du programme\n");
  return 0;
}
