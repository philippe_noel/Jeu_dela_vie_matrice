#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h> //import pour fonction sleep

#include "fonctions.h"

int** initialiserTableau(int nline, int nrow) {
  
  int** tableau = (int **)malloc(nline*sizeof(int *));

  for (int k=0; k<nline; k++) {
    tableau[k] = (int*) malloc(nrow*sizeof(int));
    for (int j=0; j<nrow; j++) {
	  tableau[k][j] = 0;
    }
  } 
  return tableau;
}

void afficherTableau(int** tab, int nline, int nrow) {
  move(0, 0);
  for (int ii = 0; ii < nline; ii++) {
    for (int jj = 0; jj < nrow; jj++) {
      if (tab[ii][jj]) {
	printw("%c ", 64);
      } else {
	printw("%c ", 32);
      }
    }
    printw("\n");
  }
  refresh();

}

int calculerVoisin(int **tab_temp, int row, int line) {

  int count = tab_temp[row-1][line-1] + tab_temp[row-1][line] + tab_temp[row-1][line+1] + tab_temp[row][line-1] + tab_temp[row][line+1] + tab_temp[row+1][line-1] + tab_temp[row+1][line] + tab_temp[row+1][line];
  
  return count;
}

void modifierMatrice(int **tab, int **tab_temp, int nrow, int nline) {

  int nb_voisin = 0;
  int temp = 0;
  for (int k = 1; k < nline-1; k++) {
    for (int j = 1; j < nrow-1; j++) {
      nb_voisin = calculerVoisin(tab, j, k);
      if (nb_voisin == 3) {
	tab_temp[k][j] = 1;
      } else if (nb_voisin == 2) {
	temp = tab[k][j];
	tab_temp[k][j] = temp;
      } else {
	tab_temp[k][j] = 0;
      }
    }
  }
}

void randomTableau(int **tableau, int nline, int nrow) {

  for (int k = 1; k < nline-1; k++) {
    for (int j = 1; j < nrow-1; j++) {
      tableau[k][j] = rand() % 2;
    }
  } 
}
