#ifndef DEF_FONCTIONS
#define DEF_FONCTIONS

int ** initialiserTableau(int nline, int nrow);
void afficherTableau(int** tab, int nline, int nrow);
void modifierMatrice(int **tab, int **tab_temp, int nrow, int nline);
void randomTableau(int **tableau, int nline, int nrow);
int calculerVoisin(int **tab_temp, int row, int line);

#endif 
